#
# v4/develop branch
# 
FROM alpine:3.17
ARG SONARR_BRANCH='develop'
ARG SONARR_CHANNEL='v4-preview'

LABEL org.opencontainers.image.authors = "cusoon"
LABEL org.opencontainers.image.source = "https://gitlab.com/homesrvr/docker-sonarr"

SHELL ["/bin/ash", "-o", "pipefail", "-c"]
RUN \
    apk --no-cache add curl jq libmediainfo icu-libs sqlite-libs xmlstarlet bash && \
    mkdir -p /app/sonarr/bin && \
    SONARR_VERSION=$(curl -sk http://services.sonarr.tv/v1/releases | jq -r '."v4-preview".version') && \
    curl -ko /tmp/sonarr.tar.gz -L "https://services.sonarr.tv/v1/update/${SONARR_BRANCH}/download?version=${SONARR_VERSION}&os=linuxmusl&arch=x64" && \
    tar xf /tmp/sonarr.tar.gz -C /app/sonarr/bin --strip-components=1 && \
    rm -rf /app/sonarr/bin/Sonarr.Update && \
    find . -name '*.mdb' -delete && \
    rm -rf /tmp/* /var/tmp/* /var/lib/apt/lists/* /var/cache/apk/*


RUN mkdir /config
COPY /testing/config.xml /config
EXPOSE 8989
VOLUME /config

HEALTHCHECK --interval=1m --timeout=5s --start-period=30s \
            CMD wget --no-check-certificate --quiet --spider 'http://localhost:8989' && echo "OK" || exit 1

ENTRYPOINT ["/app/sonarr/bin/Sonarr", "--nobrowser", "--data=/config"]

# Container labels
ARG BUILD_DATE
ARG BUILD_REF
LABEL org.opencontainers.image.authors = "cusoon"
LABEL org.opencontainers.image.source = "https://gitlab.com/homesrvr/docker-sonarr"
LABEL org.label-schema.build-date=${BUILD_DATE}
LABEL org.label-schema.vcs-ref=${BUILD_REF} 
LABEL org.label-schema.name="Sonarr"
LABEL org.label-schema.schema-version="$SONARR_VERSION"