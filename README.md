# docker-sonarr

[![pipeline status](https://gitlab.com/homesrvr/docker-sonarr/badges/main/pipeline.svg)](https://gitlab.com/homesrvr/docker-sonarr/commits/main) 
[![Based on sonarr release](https://gitlab.com/homesrvr/docker-sonarr/-/jobs/artifacts/main/raw/release.svg?job=publish_badge)](https://gitlab.com/homesrvr/docker-sonarr/-/jobs/artifacts/main/raw/release.txt?job=publish_badge)
[![Docker image](https://gitlab.com/homesrvr/docker-sonarr/-/jobs/artifacts/main/raw/dockerimage.svg?job=publish_badge)](https://hub.docker.com/r/culater/sonarr)
[![](https://img.shields.io/docker/image-size/culater/sonarr/latest)](https://hub.docker.com/r/culater/sonarr)
[![](https://img.shields.io/docker/pulls/culater/sonarr?color=%23099cec)](https://hub.docker.com/r/culater/sonarr)


The [Sonarr PVR](https://sonarr.tv/ "Sonarr Project Homepage") , dockerized.

This is part of a collection of docker images, designed to run on my low-end x86 based QNAP NAS server. My aim is to keep this a lightweight image.

## Example usage

**docker run example**
```yaml
docker run -d \
  -v <directory where to store config>:/config \                           # directory where the config file is stored
  -v <directory where to store tv series files>:/tv \                      # directory where the tv series folder is maintained
  -v <directory where the download client stores files>:/downloads \       # directory where new downloads arrive
  -p 8989:8989 \
  --restart=unless-stopped culater/sonarr
```

**docker-compose example:**
```yaml
---
version: "3"
services:
    sonarr:
        image: culater/sonarr:latest
        container_name: sonarr
        environment:
        - PUID=1000
        - PGID=100
        - TZ=Europe/Berlin
        volumes:
        - <directory where to store config>:/config
        - <directory where to store tv series files>:/tv
        - <directory where the download client stores files>:/downloads
        ports:
        - 8989:8989
        restart: unless-stopped
```

## Reporting problems
Please report any issues to the [Gitlab issue tracker](https://gitlab.com/homesrvr/docker-sonarr/-/issues)

## Authors and acknowledgment
More information about sonarr can be found here:
[Sonarr](https://sonarr.tv/ "Sonarr Project Homepage") 


## Project status
This image is being updated on a weekly schedule and should catch up with new releases of sonarr within a few days. 